<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->id();
            $table->string('fullname')->nullable();
            $table->string('type')->nullable();
            $table->string('photo')->nullable();
            $table->string('designation')->nullable();
            $table->text('message')->nullable();
            $table->string('document')->nullable();
            $table->string('institution')->nullable();
            $table->date('registered_on')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
