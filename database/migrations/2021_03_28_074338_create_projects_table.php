<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->text('details')->nullable();
            $table->string('images')->nullable();
            $table->string('type')->nullable();
            $table->string('location')->nullable();
            $table->string('sector')->nullable();
            $table->string('investment')->nullable();
            $table->boolean('status')->nullable();
            $table->date('approved_on')->nullable();
            $table->string('initiated_by')->nullable();
            $table->date('published_on')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
