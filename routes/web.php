<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\DeleteController;
use App\Http\Controllers\GetController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//frontend
Route::get('/',[GetController::class,'homePage']);
// Route::get('layouts/app',[GetController::class,'footer']);
// Route::get('footer',function(){
// 	echo 'hello';die;
// 	// return view('wow');
// });


Route::get('/notice/{id}',[GetController::class,'singleNotice']);
Route::get('/notice',[GetController::class,'noticeList']);

Route::get('/event/{id}',[GetController::class,'singleEvent']);
Route::get('/event',[GetController::class,'eventList']);

Route::get('/enews/{id}',[GetController::class,'singleNews']);
Route::get('/enews',[GetController::class,'newsList']);

Route::get('/project/{id}',[GetController::class,'singleProject']);
Route::get('/project',[GetController::class,'projectList']);
//Dashboard
//### views


Route::get('/admin', function(){
	return view('dashboard/index');
});

Route::get('/noticeform', function(){
	return view('dashboard/notice/addnotice');
});

Route::get('/eventform', function(){
	return view('dashboard/events/addevent');
});

Route::get('/newsform', function(){
	return view('dashboard/news/addnews');
});

Route::get('/projectform', function(){
	return view('dashboard/project/addproject');
});

Route::get('/staffform', function(){
	return view('dashboard/staff/addstaff');
});

//### gets
Route::get('notices',[GetController::class,'getNotice']);
Route::get('events',[GetController::class,'getEvent']);
Route::get('news',[GetController::class,'getNews']);
Route::get('projects',[GetController::class,'getProject']);
Route::get('staffs',[GetController::class,'getStaff']);





//###ajax
Route::post('onenotice',[GetController::class,'oneNot']);
Route::post('delnotice',[DeleteController::class,'delNotice']);

Route::post('oneevent',[GetController::class,'oneEve']);
Route::post('delevent',[DeleteController::class,'delEvent']);


Route::post('onenews',[GetController::class,'oneNews']);
Route::post('delnews',[DeleteController::class,'delNews']);

Route::post('onestaff',[GetController::class,'oneStaff']);
Route::post('delstaff',[DeleteController::class,'delStaff']);

Route::post('oneproject',[GetController::class,'oneProject']);
Route::post('delproject',[DeleteController::class,'delProject']);

//###posts
Route::post('/addnotice',[PostController::class,'addNotice']);
Route::post('/addevent',[PostController::class,'addEvent']);
Route::post('/addnews',[PostController::class,'addNews']);
Route::post('/addproject',[PostController::class,'addProject']);
Route::post('/addstaff',[PostController::class,'addStaff']);






