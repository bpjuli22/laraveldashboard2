$(window).on('load', function() {
    $('#myModal').modal('show');
});

$(document).ready(function () {
 	$(".hs-menubar").hsMenu(); 
 });

$(document).ready(function(){
	$(window).scroll(function () {
	if ($(this).scrollTop() > 50) {
		$('#back-to-top').fadeIn();
	} else {
		$('#back-to-top').fadeOut();
	}
});
// scroll body to 0px on click
$('#back-to-top').click(function () {
	$('body,html').animate({
		scrollTop: 0
	}, 400);
	return false;
});
});

new WOW().init();

$('#homeSlider').owlCarousel({
	animateIn: 'fadeIn',
	animateOut: 'fadeOut',
	loop:true,
	dots:true,
	nav:false,
	autoplay:true,
	autoplayTimeout:4000,
    smartSpeed: 500,
	autoplayHoverPause: false,
	responsive:{
		0:{
			items:1,
			dots:false
		},
		600:{
			items:1,
            dots:false
		},
		1000:{
			items:1
		}
	}
})

$('#testimonialSlider').owlCarousel({
    margin:25,
    loop:true,
    dots:false,
    nav:true,
    smartSpeed:1000,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause: true,
    navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:2
        }
    }
})