<?php 
//single event page
use Carbon\Carbon;
 ?>

@extends('layouts.app')

@section('title', 'single news details or tags')
@section('content')
<div class="main-body">
				
				<section id="page-banner">
					<div class="container">
						<div class="page-title-bar">
							<div class="page-title">
								PIA News
							</div>
							<div class="page-breadcumb">
								<a href="/">Home</a> > <a href="/enews">Resources</a> > <a href="#">PIA News</a>
							</div>
						</div>
					</div>
				</section>

				<section id="singleNews" class="page-padd">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<h4>
									{{$news->title}}
								</h4>
								<div class="text-right mb-3">
									{{Carbon::parse($news->published_on)->format('d M, Y')}}
								</div>
								<img class="img-fluid mb-4" src="{{$news->image}}">
								<div class="news-body">
									<p>
										{{nl2br($news->details)}}
									</p>
									
								</div>
								<div class="text-right">
									<!-- <em>- Author's Name Here</em> -->
								</div>
							</div>
							<div class="col-md-4">
								<div class="news-sidebar">
									<h4>Latest News</h4>
									<hr>
									@foreach($lnews as $lnews)
									<div class="latest-news-block">
										<div class="latest-news-left">
											<a href="/enews/{{$lnews->id}}">
												<img src="{{$lnews->image}}">
											</a>
										</div>
										<div class="latest-news-right">
											<a href="/enews/{{$lnews->id}}">
												<h6 class="latest-news-title">
													{{substr($lnews->details,0,82).'...'}}
												</h6>
												<div class="latest-news-date">
													{{Carbon::parse($lnews->published_on)->format('M d, Y')}}
												</div>
											</a>
										</div>
									</div>
									@endforeach
									<div class="latest-news-block">
										<div class="latest-news-left">
											<a href="singlenews.html">
												<img src="img/s3.jpg">
											</a>
										</div>
										<div class="latest-news-right">
											<a href="singlenews.html">
												<h6 class="latest-news-title">
													Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												</h6>
												<div class="latest-news-date">
													Mar 10, 2021
												</div>
											</a>
										</div>
									</div>
									<div class="latest-news-block">
										<div class="latest-news-left">
											<a href="singlenews.html">
												<img src="img/s4.jpg">
											</a>
										</div>
										<div class="latest-news-right">
											<a href="singlenews.html">
												<h6 class="latest-news-title">
													Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												</h6>
												<div class="latest-news-date">
													Mar 03, 2021
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div>
@endsection