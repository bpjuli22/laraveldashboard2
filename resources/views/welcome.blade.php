<?php 
use Carbon\Carbon;
 ?>

@extends('layouts.app')

@section('title', 'Home Page')



@section('content')
    
            <div class="main-body">
                <section id="page-banner">
                    <div class="container">
                        <div class="page-title-bar">
                            <div class="page-title">
                                title
                            </div>
                            <div class="page-breadcumb">
                                <a href="index.html">Home</a> > <a href="index.html">About IB P1</a> > <a href="#">Title</a>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="homeSliderSection">
                    <div class="owl-carousel owl-theme" id="homeSlider">
                        <div class="item">
                            <img src="img/Everest.jpg">
                            <div class="slider-text">World's Highest Mountain, Mt. Everest</div>
                        </div>
                        <div class="item">
                            <img src="img/s02.jpg">
                            <div class="slider-text">Mata Pathivara, Taplejung</div>
                        </div>
                        <div class="item">
                            <img src="img/s003.jpg">
                            <div class="slider-text">Best Tea Farming in Nepal, Illam</div>
                        </div>
                        <div class="item">
                            <img src="img/s04.jpg">
                            <div class="slider-text">Koshi Tappu Wildlife Reserve, Koshi</div>
                        </div>
                    </div>
                </section>

                <section class="section-padd">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="body-blocks">
                                    <div class="row">
                                        <div class="col-md-5 order-sm-1 order-2">
                                            <div class="red-heading">
                                                Board Members
                                            </div>
                                            <div class="board-list">
                                                @if(count($bmem)>0)
                                                    @foreach($bmem as $bmem)
                                                    <div class="member-block">
                                                    <div class="member-block-img">
                                                        <img src="{{$bmem->photo}}">
                                                    </div>
                                                    <div class="member-block-txt">
                                                        <div class="member-block-name">
                                                           {{$bmem->fullname}}
                                                        </div>
                                                        <div class="member-block-post">
                                                           {{$bmem->designation}}
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @endif
                                                <div class="member-block">
                                                    <div class="member-block-img">
                                                        <img src="img/man.jpg">
                                                    </div>
                                                    <div class="member-block-txt">
                                                        <div class="member-block-name">
                                                            Full Name Here
                                                        </div>
                                                        <div class="member-block-post">
                                                            Post/ Designation
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="member-block">
                                                    <div class="member-block-img">
                                                        <img src="img/man.jpg">
                                                    </div>
                                                    <div class="member-block-txt">
                                                        <div class="member-block-name">
                                                            Full Name Here
                                                        </div>
                                                        <div class="member-block-post">
                                                            Post/ Designation
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="member-block">
                                                    <div class="member-block-img">
                                                        <img src="img/man.jpg">
                                                    </div>
                                                    <div class="member-block-txt">
                                                        <div class="member-block-name">
                                                            Full Name Here
                                                        </div>
                                                        <div class="member-block-post">
                                                            Post/ Designation
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="member-block">
                                                    <div class="member-block-img">
                                                        <img src="img/man.jpg">
                                                    </div>
                                                    <div class="member-block-txt">
                                                        <div class="member-block-name">
                                                            Full Name Here
                                                        </div>
                                                        <div class="member-block-post">
                                                            Post/ Designation
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="member-block">
                                                    <div class="member-block-img">
                                                        <img src="img/man.jpg">
                                                    </div>
                                                    <div class="member-block-txt">
                                                        <div class="member-block-name">
                                                            Full Name Here
                                                        </div>
                                                        <div class="member-block-post">
                                                            Post/ Designation
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-7 order-1">
                                            <div class="gray-heading">
                                                Notice/Announcement
                                            </div>
                                            <div class="list-box">
                                                <ul class="board-ul">
                                                    @foreach($not as $not)
                                                        <li>
                                                        <a href="notice/{{$not->id}}">
                                                            {{substr($not->title,0,200).'..'}}
                                                        </a>
                                                    </li>
                                                    @endforeach
                                                    <li>
                                                        <a href="singlenotice.html">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                        </a>
                                                    </li>
                                                    
                                                   
                                                   
                                                </ul>
                                                <div class="text-right">
                                                    <a href="notice">View All Announcements...</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="red-heading">
                                                Latest Events
                                            </div>
                                            <div class="list-box">
                                                <ul class="events-ul">
                                                   @if(count($event)>0)
                                                        @foreach($event as $event)
                                                         <li>
                                                        <a href="event/{{$event->id}}">
                                                            <div class="events-ul-title">
                                                               {{$event->title}}
                                                            </div>
                                                            <div class="events-ul-date">
                                                              {{Carbon::parse($event->published_on)->format('M d, Y')}}
                                                            </div>
                                                        </a>
                                                    </li>
                                                    @endforeach
                                                   @endif
                                                    <li>
                                                        <a href="singleevent.html">
                                                            <div class="events-ul-title">
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                            </div>
                                                            <div class="events-ul-date">
                                                                Feb 23, 2021
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="singleevent.html">
                                                            <div class="events-ul-title">
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                            </div>
                                                            <div class="events-ul-date">
                                                                Feb 23, 2021
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="singleevent.html">
                                                            <div class="events-ul-title">
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                            </div>
                                                            <div class="events-ul-date">
                                                                Feb 23, 2021
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="singleevent.html">
                                                            <div class="events-ul-title">
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                            </div>
                                                            <div class="events-ul-date">
                                                                Feb 23, 2021
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="singleevent.html">
                                                            <div class="events-ul-title">
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                            </div>
                                                            <div class="events-ul-date">
                                                                Feb 23, 2021
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="singleevent.html">
                                                            <div class="events-ul-title">
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                            </div>
                                                            <div class="events-ul-date">
                                                                Feb 23, 2021
                                                            </div>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="text-right">
                                                    <a href="event">View All Events...</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="p1-map">
                                                <img class="w-100" src="img/p1.jpg">
                                                <img class="w-100 px-5" src="img/nepalp1.jpg">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="social-container">
                                    <div class="facebook mb-4">
                                        <div class="fb-page" 
                                        data-href="https://www.facebook.com/facebookapp" 
                                        data-tabs="timeline, events" 
                                        data-width=""
                                        data-height="500" 
                                        data-small-header="false" 
                                        data-adapt-container-width="true" 
                                        data-hide-cover="false" 
                                        data-show-facepile="true">
                                        <blockquote cite="https://www.facebook.com/facebookapp" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebookapp">Facebook App</a></blockquote></div>
                                    </div>
                                    <div class="twitter">
                                        <a class="twitter-timeline" href="https://twitter.com/ChelseaFC?ref_src=twsrc%5Etfw" data-width="" data-height="600">Tweets by ChelseaFC</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="mb-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="red-heading">
                                    Latest News & Updates
                                </div>
                                <div class="list-box">
                                    <ul class="news-ul">
                                       
                                            @foreach($news as $news)
                                             <li>
                                                <a href="enews/{{$news->id}}">
                                                <div class="news-ul-title">
                                                    {{substr($news->title,0,210).'...'}}
                                                </div>
                                                <div class="news-ul-date">
                                                    {{Carbon::parse($event->published_on)->format('M d, Y')}}
                                                </div>
                                            </a>
                                            </li>
                                            @endforeach
                                        
                                        <li>
                                            <a href="singlenews.html">
                                                <div class="news-ul-title">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                </div>
                                                <div class="news-ul-date">
                                                    Feb 23, 2021
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singlenews.html">
                                                <div class="news-ul-title">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                </div>
                                                <div class="news-ul-date">
                                                    Feb 23, 2021
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singlenews.html">
                                                <div class="news-ul-title">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                </div>
                                                <div class="news-ul-date">
                                                    Feb 23, 2021
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singlenews.html">
                                                <div class="news-ul-title">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                </div>
                                                <div class="news-ul-date">
                                                    Feb 23, 2021
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singlenews.html">
                                                <div class="news-ul-title">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                </div>
                                                <div class="news-ul-date">
                                                    Feb 23, 2021
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="text-right">
                                        <a href="enews">View All News...</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="gray-heading">
                                    Popular Projects
                                </div>
                                <div class="list-box">
                                    <ul class="services-ul">
                                        @foreach($pro as $pro)
                                            <li>
                                            <a href="project/{{$pro->id}}/{{$pro->type}}">
                                               {{$pro->title}}
                                            </a>
                                        </li>
                                        @endforeach
                                        <li>
                                            <a href="singleproject.html">
                                                Project Title Here
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singleproject.html">
                                                Project Title Here
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singleproject.html">
                                                Project Title Here
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singleproject.html">
                                                Project Title Here
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singleproject.html">
                                                Project Title Here
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singleproject.html">
                                                Project Title Here
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singleproject.html">
                                                Project Title Here
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singleproject.html">
                                                Project Title Here
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singleproject.html">
                                                Project Title Here
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singleproject.html">
                                                Project Title Here
                                            </a>
                                        </li>
                                        <li>
                                            <a href="singleproject.html">
                                                Project Title Here
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="text-right">
                                        <a href="projectlisting.html">View All Projects...</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="blue-heading">
                                    Frequently Filled Forms
                                </div>
                                <div class="list-box">
                                    <ul class="forms-ul">
                                        <li>
                                            <a href="img/test.pdf" target="_blank" download>
                                                Lorem ipsum dolor sit amet
                                            </a>
                                        </li>
                                        <li>
                                            <a href="img/test.pdf" target="_blank" download>
                                                Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
                                            </a>
                                        </li>
                                        <li>
                                            <a href="img/test.pdf" target="_blank" download>
                                                Lorem ipsum dolor sit amet
                                            </a>
                                        </li>
                                        <li>
                                            <a href="img/test.pdf" target="_blank" download>
                                                Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
                                            </a>
                                        </li>
                                        <li>
                                            <a href="img/test.pdf" target="_blank" download>
                                                Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
                                            </a>
                                        </li>
                                        <li>
                                            <a href="img/test.pdf" target="_blank" download>
                                                Lorem ipsum dolor sit amet
                                            </a>
                                        </li>
                                        <li>
                                            <a href="img/test.pdf" target="_blank" download>
                                                Lorem ipsum dolor sit amet
                                            </a>
                                        </li>
                                        <li>
                                            <a href="img/test.pdf" target="_blank" download>
                                                Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
                                            </a>
                                        </li>
                                        <li>
                                            <a href="img/test.pdf" target="_blank" download>
                                                Lorem ipsum dolor sit amet
                                            </a>
                                        </li>
                                        <li>
                                            <a href="img/test.pdf" target="_blank" download>
                                                Lorem ipsum dolor sit amet
                                            </a>
                                        </li>
                                        <li>
                                            <a href="img/test.pdf" target="_blank" download>
                                                Lorem ipsum dolor sit amet
                                            </a>
                                        </li>
                                        <li>
                                            <a href="img/test.pdf" target="_blank" download>
                                                Lorem ipsum dolor sit amet
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="text-right">
                                        <a href="formslist.html">View All Forms...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
@endsection