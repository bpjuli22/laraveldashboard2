<?php 
// events list
 use Carbon\Carbon;
 ?>

@extends('layouts.app')

@section('title', 'events contents')

 @section('content')
<div class="main-body">
				
				<section id="page-banner">
					<div class="container">
						<div class="page-title-bar">
							<div class="page-title">
								Events
							</div>
							<div class="page-breadcumb">
								<a href="/">Home</a> > <a href="/">Events</a>
							</div>
						</div>
					</div>
				</section>

				<section id="blogList" class="page-padd">
					<div class="container">
						<div class="row">
							@foreach($all as $alleve)
								<div class="col-md-4">
								<div class="news-block">
									<div class="news-block-img-wrap">
										<img src="{{$alleve->image}}">
										<div class="news-block-date">
											<div class="news-block-day">
												{{Carbon::parse($alleve->event_date)->format('d')}}
											</div>
											<div class="news-block-mnth">
												{{Carbon::parse($alleve->event_date)->format('M')}}
											</div>
										</div>
									</div>
									<div class="news-block-txt">
										<h5>
											{{$alleve->title}}
										</h5>
										<div class="news-block-excerpt">
											{{substr($alleve->details,0,255).'...'}}
										</div>
										<div class="news-block-btn">
											<a href="event/{{$alleve->id}}">Read More</a>
										</div>
									</div>
								</div>
							</div>
							@endforeach
							<div class="col-md-4">
								<div class="news-block">
									<div class="news-block-img-wrap">
										<img src="img/s2.jpg">
										<div class="news-block-date">
											<div class="news-block-day">
												28
											</div>
											<div class="news-block-mnth">
												FEB
											</div>
										</div>
									</div>
									<div class="news-block-txt">
										<h5>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										</h5>
										<div class="news-block-excerpt">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
										</div>
										<div class="news-block-btn">
											<a href="singleevents.html">Read More</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="news-block">
									<div class="news-block-img-wrap">
										<img src="img/s3.jpg">
										<div class="news-block-date">
											<div class="news-block-day">
												23
											</div>
											<div class="news-block-mnth">
												FEB
											</div>
										</div>
									</div>
									<div class="news-block-txt">
										<h5>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										</h5>
										<div class="news-block-excerpt">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
										</div>
										<div class="news-block-btn">
											<a href="singleevents.html">Read More</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="news-block">
									<div class="news-block-img-wrap">
										<img src="img/Everest.jpg">
										<div class="news-block-date">
											<div class="news-block-day">
												03
											</div>
											<div class="news-block-mnth">
												MAR
											</div>
										</div>
									</div>
									<div class="news-block-txt">
										<h5>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										</h5>
										<div class="news-block-excerpt">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
										</div>
										<div class="news-block-btn">
											<a href="singleevents.html">Read More</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="news-block">
									<div class="news-block-img-wrap">
										<img src="img/s2.jpg">
										<div class="news-block-date">
											<div class="news-block-day">
												28
											</div>
											<div class="news-block-mnth">
												FEB
											</div>
										</div>
									</div>
									<div class="news-block-txt">
										<h5>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										</h5>
										<div class="news-block-excerpt">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
										</div>
										<div class="news-block-btn">
											<a href="singleevents.html">Read More</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="news-block">
									<div class="news-block-img-wrap">
										<img src="img/s3.jpg">
										<div class="news-block-date">
											<div class="news-block-day">
												23
											</div>
											<div class="news-block-mnth">
												FEB
											</div>
										</div>
									</div>
									<div class="news-block-txt">
										<h5>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										</h5>
										<div class="news-block-excerpt">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
										</div>
										<div class="news-block-btn">
											<a href="singleevents.html">Read More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="my-3 text-center">
							{{$all->links()}}
						</div>
					</div>
				</section>

			</div>
 @endsection