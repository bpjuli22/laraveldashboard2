<?php 
//selected notice page
use Carbon\Carbon;
 ?>

@extends('layouts.app')

@section('title', 'notice contents')
@section('content')
<div class="main-body">
				
				<section id="page-banner">
					<div class="container">
						<div class="page-title-bar">
							<div class="page-title">
								Notice/ Announcements
							</div>
							<div class="page-breadcumb">
								<a href="/">Home</a> > <a href="">Notice/ Announcements</a>
							</div>
						</div>
					</div>
				</section>

				<section id="singleNotice" class="page-padd">
					<div class="container">
						<div class="row">
							<div class="col-md-7">
								<div class="notice-block">
									<h5 class="single-notice-title">
										{{$not->title}}
									</h5>
									<div class="text-right mb-3">
										<em>Date -  {{Carbon::parse($not->published_on)->format('M d, Y')}}</em>
									</div>
									<p>
										{{$not->details}}
									</p>
										
								</div>
							</div>
							<div class="col-md-5">
								<a href="img/test.pdf" target="_blank" download>Download PDF <i class="fa fa-file-pdf"></i></a>
							</div>
						</div>
								
					</div>
				</section>
</div>
@endsection