<?php 
//project page
use Carbon\Carbon;
 ?>

@extends('layouts.app')

@section('title', 'project details or tags')
@section('content')
<div class="main-body">
				
				<section id="page-banner">
					<div class="container">
						<div class="page-title-bar">
							<div class="page-title">
								Projects Under Survey/ Study
							</div>
							<div class="page-breadcumb">
								<a href="/">Home</a> > <a href="/project">PIA Projects</a> > <a href="#">Under Survey/ Study</a>
							</div>
						</div>
					</div>
				</section>

				<section id="projectListing" class="page-padd">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="project-block">
									<div class="project-title">
										Title of Project 1
									</div>
									<div class="">
										<b>Location:</b> Generation (Dam Site, Powerhouse): Sankhuwasabha, Transmission: Sankhuwasabha, Bhojpur, Khotang, Udaypur, Siraha, Dhanusa, Mahottari
									</div>
									<div class="project-block-btn">
										<a href="singleproject.html">View Details</a>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="project-block">
									<div class="project-title">
										Title of Project 2
									</div>
									<div class="">
										<b>Location:</b> Generation (Dam Site, Powerhouse): Sankhuwasabha, Transmission: Sankhuwasabha, Bhojpur, Khotang, Udaypur, Siraha, Dhanusa, Mahottari
									</div>
									<div class="project-block-btn">
										<a href="singleproject.html">View Details</a>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="project-block">
									<div class="project-title">
										Title of Project 3
									</div>
									<div class="">
										<b>Location:</b> Generation (Dam Site, Powerhouse): Sankhuwasabha, Transmission: Sankhuwasabha, Bhojpur, Khotang, Udaypur, Siraha, Dhanusa, Mahottari
									</div>
									<div class="project-block-btn">
										<a href="singleproject.html">View Details</a>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="project-block">
									<div class="project-title">
										Title of Project 4
									</div>
									<div class="">
										<b>Location:</b> Generation (Dam Site, Powerhouse): Sankhuwasabha, Transmission: Sankhuwasabha, Bhojpur, Khotang, Udaypur, Siraha, Dhanusa, Mahottari
									</div>
									<div class="project-block-btn">
										<a href="singleproject.html">View Details</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div>

@endsection