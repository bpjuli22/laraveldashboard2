<!-- resources/views/layouts/app.blade.php -->
<?php use Carbon\Carbon;
 ?>
<html>
    <head>
        <title>App Name - @yield('title')</title>
          <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/ui/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/ui/fontawesome/css/all.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/ui/owl.carousel.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/ui/owl.theme.default.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/ui/material-design/css/material-design-iconic-font.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/ui/hs-menu.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/ui/animate.css')}}"/>

        <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/ui/custom.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/ui/responsive.css')}}"/>
         
    </head>
    <body>
        @include('layouts.header')
         @yield('content')
          @include('layouts.footer')
               

           

      


         <script type="text/javascript" src="{{asset('assets/dist/js/ui/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/dist/js/ui/html5lightbox/html5lightbox.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/dist/js/ui/popper.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/dist/js/ui/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/dist/js/ui/owl.carousel.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/dist/js/ui/jquery.hsmenu.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/dist/js/ui/wow.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/dist/js/ui/custom.js')}}"></script>
    </body>
</html>
