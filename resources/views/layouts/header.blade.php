<?php 
// header page with navbar
 ?>
		<div id="fb-root"></div>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v9.0" nonce="CIPTUZPc"></script>
		<div id="app">
			<header>
				<div id="desktopMenu">
					<div class="top-header">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<div class="top-social">
										<a href="#" target="_blank">
											<i class="fab fa-facebook"></i>
										</a>
										<a href="#" target="_blank">
											<i class="fab fa-twitter"></i>
										</a>
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="top-right">
										<a href="#">English</a> | <a href="#">नेपाली</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="logo-bar">
						<div class="container">
							<div class="logo-block">
								<a href="/" class="nav-logo">
									<img class="site-logo" src="{{asset('img/logo.png')}}" alt="logo">
									<div class="logo-txt">
										<div class="nep-txt">
											प्रदेश सरकार प्रदेश लगानी प्रादिकरण
										</div>
										<div class="eng-txt">
											Province Investment Authority
										</div>
										<div class="eng-txt-address">
											Biratnagar, Morang, Province 1
										</div>
									</div>
								</a>
								<div>
									<div class="row">
										<div class="col-9 my-auto">
											<form id="search-form">
												<input type="text" name="search" placeholder="Search - Keywords">
												<button type="submit">Search</button>
											</form>
										</div>
										<div class="col-3 my-auto">
											<img class="top-flag" src="{{asset('img/flag.gif')}}" alt="flag">
										</div>
									</div>		
								</div>
							</div>
						</div>
					</div>
					<div class="menu-bar">
						<nav class="navbar navbar-expand-lg">
							<div class="container">
								<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
									<i class="fa fa-bars"></i>
								</button>			
								<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
									<ul class="navbar-nav">
										<li class="nav-item active">
											<a class="nav-link" href="index.html">Home</a>
										</li>
										<li class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">About PIA</a>
											<div class="dropdown-menu">
												<a class="dropdown-item" href="about.html">PIA</a>
												<a class="dropdown-item" href="boardmembers.html">Board Members</a>
												<a class="dropdown-item" href="chairmansmessage.html">Chairperson's Message</a>
												<a class="dropdown-item" href="CEOsmessage.html">CEO's Message</a>
												<a class="dropdown-item" href="ibp1staffs.html">PIA Staff</a>
												<a class="dropdown-item" href="consultingstaff.html">PIA Consulting Staff</a>
										  	</div>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="approvals.html">Approvals</a>
										</li>
										<li class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">PIA Projects</a>
											<div class="dropdown-menu">
												<a class="dropdown-item" href="underimplementation.html">Under Implementation</a>
												<a class="dropdown-item" href="undersurvey.html">Under Survey/Study</a>
												<a class="dropdown-item" href="underprocurement.html">Under Procurement Stage</a>
												<a class="dropdown-item" href="unsolicitedprojects.html">Unsolicited Projects</a>
											</div>
										</li>
										<li class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Investment Climate</a>
											<div class="dropdown-menu">
												<a class="dropdown-item" href="province1profile.html">Province 1 Profile</a>
												<a class="dropdown-item" href="whyinvestinp1.html">Why invest in Province 1?</a>
												<a class="dropdown-item" href="investmentprocess.html">Investment Process</a>
												<a class="dropdown-item" href="investorstestimonies.html">Investor's Testimonies</a>
											</div>
										</li>
										<li class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Opportunities</a>
											<div class="dropdown-menu">
												<a class="dropdown-item" href="projectbank.html">Project Bank</a>
											</div>								
										</li>
										<li class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Resources</a>
											<div class="dropdown-menu">
												<a class="dropdown-item" href="documentslibrary.html">Documents Library</a>
												<a class="dropdown-item" href="gallery.html">Multimedia Gallery</a>
												<a class="dropdown-item" href="news.html">PIA News</a>
												<a class="dropdown-item" href="faq.html">FAQ</a>
											</div>								
										</li>
										<li class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Notices</a>
											<div class="dropdown-menu">
												<a class="dropdown-item" href="procurementnotices.html">Procurement Notices</a>
												<a class="dropdown-item" href="vacancynotices.html">Vacancy Notices</a>
												<a class="dropdown-item" href="pressrelease.html">Press Release</a>
												<a class="dropdown-item" href="publicnotices.html">Public Notice</a>
												<a class="dropdown-item" href="billspresentation.html">Bills Presentation</a>
											</div>								
										</li>
										<li class="nav-item">
											<a class="nav-link" href="contact.html">Contact</a>
										</li>
									</ul>
								</div>
							</div>
						</nav>
					</div>
				</div>
				<div id="mobileMenu">
					<div class="hs-menubar">
						<div class="brand-logo">
				            <a href="index.html"><img src="img/mobilelogo.jpg" alt="PIA logo"></a>
				        </div>
				        <div class="menu-trigger"> <i class="fa fa-bars"></i></div>
				        <img class="mobile-top-flag" src="img/flag.gif" alt="flag">
					</div>
					<nav class="hs-navigation">
						<div class="text-right pt-2" style="color: #eee;">
		         			<a class="mob-lang" href="#">English</a>|<a class="mob-lang" href="#">Nepali</a>
		         		</div>
				         <ul class="nav-links">
				         	<li>
				         		<form id="mobSearch">
				         			<div class="mob-search-bar">
				         				<input type="text" name="searchKey" placeholder="Search...">
				         				<button type="submit" class="mob-search-btn">
				         					<i class="fa fa-search"></i>
				         				</button>
				         			</div>
				         		</form>
				         	</li>
				         	<li><a href="index.html">Home</a></li>
							<li class="has-child">
				               <span class="its-parent">About PIA</span>
				               <ul class="its-children">
									<li><a href="about.html">PIA</a> </li>
									<li><a href="boardmembers.html">Board Members</a> </li>
									<li><a href="chairmansmessage.html">Chairperson's Message</a> </li>
									<li><a href="CEOsmessage.html">CEO's Message</a> </li>
									<li><a href="ibp1staffs.html">PIA Staff</a> </li>
									<li><a href="consultingstaff.html">PIA Consulting Staff</a> </li>
								</ul>
							</li>
							<li><a href="approvals.html">Approvals</a></li>
							<li class="has-child">
				               <span class="its-parent">PIA Projects</span>
				               <ul class="its-children">
									<li><a href="underimplementation.html">Under Implementation</a> </li>
									<li><a href="undersurvey.html">Under Survey/Study</a> </li>
									<li><a href="underprocurement.html">Under Procurement Stage</a> </li>
									<li><a href="unsolicitedprojects.html">Unsolicited Projects</a> </li>
								</ul>
							</li>
							<li class="has-child">
				               <span class="its-parent">Investment Climate</span>
				               <ul class="its-children">
									<li><a href="province1profile.html">Province 1 Profile</a> </li>
									<li><a href="whyinvestinp1.html">Why invest in Province 1</a> </li>
									<li><a href="investmentprocess.html">Investment Process</a> </li>
									<li><a href="investorstestimonies.html">Investor's Testimonies</a> </li>
								</ul>
							</li>
							<li class="has-child">
				               <span class="its-parent">Oppurtunities</span>
				               <ul class="its-children">
									<li><a href="projectbank.html">Project Bank</a> </li>
								</ul>
							</li>
							<li class="has-child">
				               <span class="its-parent">Resources</span>
				               <ul class="its-children">
									<li><a href="documentslibrary.html">Documents Library</a> </li>
									<li><a href="gallery.html">Multimedia Gallery</a> </li>
									<li><a href="news.html">PIA News</a> </li>
									<li><a href="faq.html">FAQ</a> </li>
								</ul>
							</li>
							<li class="has-child">
				               <span class="its-parent">Notices</span>
				               <ul class="its-children">
									<li><a href="procurementnotices.html">Procurement Notices</a> </li>
									<li><a href="vacancynotices.html">Vacancy Notices</a> </li>
									<li><a href="pressrelease.html">Press Release</a> </li>
									<li><a href="publicnotices.html">Public Notice</a> </li>
									<li><a href="billspresentation.html">Bills Presentation</a> </li>
								</ul>
							</li>
							<li><a href="contact.html">Contact</a></li>
						</ul>
					</nav>
				</div>
			</header>
		