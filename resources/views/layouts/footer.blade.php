<?php 
// footer page with news and links
use Carbon\Carbon;
 ?>
 <footer>
                <div class="footer-menu">
                    <nav class="navbar navbar-expand-lg">
                        <div class="container">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler02" aria-controls="navbarToggler02" aria-expanded="false" aria-label="Toggle navigation">
                                Quick Menu <i class="fa fa-chevron-down"></i>
                            </button>           
                            <div class="collapse navbar-collapse" id="navbarToggler02">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link" href="about.html">About Us</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="contact.html">Contact Us</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="feedback.html">Feedback</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="faq.html">Help</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="policypage.html">Website Policy</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Sitemap</a>                            
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div id="preFooter">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 first">
                                <div class="footer-title">Quick Links</div>
                                <div class="footer-li"><a href="documentslibrary.html">Documents Library</a></div>
                                <div class="footer-li"><a href="gallery.html">Gallery</a></div>
                                <div class="footer-li"><a href="boardmembers.html">Board Members</a></div>
                                <div class="footer-li"><a href="province1profile.html">Province 1 Profile</a></div>
                                <div class="footer-li"><a href="investmentprocess.html">Investment Process</a></div>
                                <div class="footer-li"><a href="faq.html">FAQ</a></div>
                            </div>
                            <div class="col-md-4 second">
                                <div class="footer-title">Latest News</div>
                                <div class="footer-news">
                                    @foreach($fnews as $fnews)
                                    <div class="footer-news-block">
                                        <div class="footer-news-left">
                                            <a href="singlenews.html">
                                                <img class="footer-news-img" src="{{$fnews->image}}">
                                            </a>
                                        </div>
                                        <div class="footer-news-right">
                                            <a href="singlenews/{{$fnews->id}}">
                                                <div class="footer-news-title">
                                                    {{$fnews->title}}
                                                </div>
                                                <div class="footer-news-date">
                                                    {{Carbon::parse($fnews->published_on)->format('M d, Y')}}
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                  </div>
                                </div>
                          
                            <div class="col-md-4 third">
                                <div class="footer-title">Contact Details</div>
                                <div class="footer-li">
                                    Biratnagar, Morang, Nepal
                                </div>
                                <div class="footer-li">
                                    Tel: +977-21-1234567/78/89
                                </div>
                                <div class="footer-li">
                                    Fax: +977-21-1234521
                                </div>
                                <div class="footer-li">
                                    Mail: info@ibmp1.gov.np
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="copyright">
                    <div class="container">
                        © Province Investment Authority 2021. All Rights Reserved.
                    </div>
                </div>
 </footer>