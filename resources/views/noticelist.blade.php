<?php 
// all notices
 use Carbon\Carbon;
 ?>

@extends('layouts.app')

@section('title', 'notice contents')
@section('content')
 <div class="main-body">
				
				<section id="page-banner">
					<div class="container">
						<div class="page-title-bar">
							<div class="page-title">
								Notices
							</div>
							<div class="page-breadcumb">
								<a href="index.html">Home</a> > <a href="#">Notices</a>
							</div>
						</div>
					</div>
				</section>

				<section id="noticeList" class="page-padd">
					<div class="container">
						<ul class="noticelist-ul">
							@foreach($all as $allnot)
								<li>
								<div class="notice-flex">
									<div class="notice-title">
										<a href="notice/{{$allnot->id}}">
											{{$allnot -> title}}
										</a>
									</div>
									<div class="notice-date">
										{{$allnot -> published_on}}
									</div>
								</div>

							</li>
							@endforeach
							
							 <li>
								<div class="notice-flex">
									<div class="notice-title">
										<a href="singlenotice.html">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod ...
										</a>
									</div>
									<div class="notice-date">
										23/03/2021
									</div>
								</div>
							</li>
							<li>
								<div class="notice-flex">
									<div class="notice-title">
										<a href="singlenotice.html">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod ...
										</a>
									</div>
									<div class="notice-date">
										23/03/2021
									</div>
								</div>
							</li>
							<li>
								<div class="notice-flex">
									<div class="notice-title">
										<a href="singlenotice.html">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod ...
										</a>
									</div>
									<div class="notice-date">
										23/03/2021
									</div>
								</div>
							</li>
							<li>
								<div class="notice-flex">
									<div class="notice-title">
										<a href="singlenotice.html">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod ...
										</a>
									</div>
									<div class="notice-date">
										23/03/2021
									</div>
								</div>
							</li>
							<li>
								<div class="notice-flex">
									<div class="notice-title">
										<a href="singlenotice.html">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod ...
										</a>
									</div>
									<div class="notice-date">
										23/03/2021
									</div>
								</div>
							</li>
							<li>
								<div class="notice-flex">
									<div class="notice-title">
										<a href="singlenotice.html">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod ...
										</a>
									</div>
									<div class="notice-date">
										23/03/2021
									</div>
								</div>
							</li>
							<li>
								<div class="notice-flex">
									<div class="notice-title">
										<a href="singlenotice.html">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod ...
										</a>
									</div>
									<div class="notice-date">
										23/03/2021
									</div>
								</div>
							</li>
							<li>
								<div class="notice-flex">
									<div class="notice-title">
										<a href="singlenotice.html">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod ...
										</a>
									</div>
									<div class="notice-date">
										23/03/2021
									</div>
								</div>
							</li>
							<li>
								<div class="notice-flex">
									<div class="notice-title">
										<a href="singlenotice.html">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod ...
										</a>
									</div>
									<div class="notice-date">
										23/03/2021
									</div>
								</div>
							</li>
							<li>
								<div class="notice-flex">
									<div class="notice-title">
										<a href="singlenotice.html">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod ...
										</a>
									</div>
									<div class="notice-date">
										23/03/2021
									</div>
								</div>
							</li>
							<li>
								<div class="notice-flex">
									<div class="notice-title">
										<a href="singlenotice.html">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod ...
										</a>
									</div>
									<div class="notice-date">
										23/03/2021
									</div>
								</div>
							</li>
						</ul>
						<div class="mt-4">
							<!-- PAGINATION HERE -->
							{{ $all->links() }}
						</div>

					</div>
				</section>

			</div>
@endsection