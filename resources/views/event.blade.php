<?php 
//single event page
use Carbon\Carbon;
 ?>

@extends('layouts.app')

@section('title', 'single events details or tags')
@section('content')
 			<div class="main-body">
				
				<section id="page-banner">
					<div class="container">
						<div class="page-title-bar">
							<div class="page-title">
								Events
							</div>
							<div class="page-breadcumb">
								<a href="/">Home</a> > <a href="event">Events</a>
							</div>
						</div>
					</div>
				</section>

				<section id="singleNotice" class="page-padd">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<div class="notice-block">
									<h5 class="single-notice-title">
										{{$eve->title}}
									</h5>
									<div class="text-right mb-3">
										<em>Published Date - {{Carbon::parse($eve->published_on)->format('M d, Y')}}</em>
									</div>
									<ul>
										<li>Event Date: {{Carbon::parse($eve->event_date)->format('M d, Y') .' to '.Carbon::parse($eve->end_date)->format('M d, Y') }} </li>
										<li>Event Time: {{Carbon::parse($eve->event_time)->format('H:i') .' to '.Carbon::parse($eve->end_time)->format('H:i')}}</li>
										<li>Event Guests: {{$eve->guest}}</li>
									</ul>
									<div class="mb-3">
										<img class="img-fluid" src="{{$eve->image}}">
									</div>
									<p>
										{{$eve->details}}
									</p>
										
								</div>
							</div>
							<div class="col-md-4">
								<a href="img/test.pdf" target="_blank" download>Download PDF <i class="fa fa-file-pdf"></i></a>

							</div>
						</div>
								
					</div>
				</section>

			</div>
			@endsection