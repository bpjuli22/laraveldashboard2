<?php 
use Illuminate\Support\Str;

 ?>

<h4 class="h4 mt-5">
	News List
</h4>
 <table class="table table-striped table-hover">
 	<thead>
 		<tr class="bg-secondary text-light mt-2">
 			<th>SN</th>
 			<th>Title</th>
 			<th>Description</th>
 			<th>Published on</th>
 			<th>
 				Changes
 			</th>

 		</tr>
 	</thead>
 	<tbody>

 		@if(count($news)>0)
 		@foreach($news as $key=>$news)
 		
 		<tr>
 			<td>{{$key+1}}</td>
 			<td>{{$news->title}}</td>
 			
 
 			<td>{{substr($news->details,0,100)}}</td>
 			<td>{{$news->published_on}}</td>
 		
 			<td> <a href="" class="text-danger delete" data-id="{{$news->id}}">Delete</a>|<a href="#" class="view text-warning" data-id="{{$news->id}}" role="button">View</a></td>

 		</tr>
 		@endforeach
 		@else
 		{{'Nothing to show'}}
 		@endif

 	</tbody>
 </table>

 <!-- Modal for news details -->
<div class="modal fade" id="news" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">
        	
        		
        		
        		
        </h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="top">
        	<img src=""  id="myimg" style="height: 300px;"/>
        </div>
        <div class="bottom">
        	
        </div>
      </div>
      <div class="modal-footer">
      	<span class="published_on"></span>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

<!-- view contents on clicking view button -->
<script type="text/javascript">
	$(document).on('click','.view',function(){
		var id=$(this).data('id');
		var myModal = new bootstrap.Modal(document.getElementById('news'), {
  keyboard: false
})
		$.ajax({
			url:"{{url('onenews')}}",
			data:{id:id ,"_token": "{{ csrf_token() }}"},
			type:'post',
			dataType:'json',
			success: function(data){
				myModal.show();
				$('#news .modal-title').html(data.onenews.title);
				$('#news .modal-body .bottom').html(data.onenews.details);
				$('#myimg').attr('src', data.onenews.image);


			}
		});
	})

	$(document).on('click','.delete',function(){
		var id=$(this).data('id');
		// alert(id)
	$.ajax({
			url:'delnews',
			data:{id:id ,"_token": "{{ csrf_token() }}"},
			type:'post',
			
			success: function(data){
				alert('Deleted successfully');

			}
		});
	})
</script>
