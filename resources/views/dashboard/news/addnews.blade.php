<?php 
//### add news page with a form
 ?>



   <h1 class="h2 my-2">Add News and Update</h1>
 <form class="form " action="/addnews" method="post" enctype="multipart/form-data">
 	@csrf
 	<div class="col-lg-10">
 		<div class="form-floating mb-3">
 			<input class="form-control" id="title" type="text" placeholder="Title" name="title" />
 		<label for="title">Title for news</label>
 		</div>

 		<div class="row">
 			<!-- <div class="col-6">
 			<select class="form-select" name="type">
 				<option>--notice type--</option>
 				<option value="pub">Public</option>
 				<option value="press">Press</option>
 				<option value="pro">Procurement</option>
 				<option value="vac">Vacancy</option>
  				<option value="bill">Bill's presentation</option>
 				

 			</select>
 		</div> -->
 		<div class="col-6">
 		<input class="form-file mt-1" type="file" name="img" />
 		</div>
 		</div>
 		
 	
 			<div class="form-floating mt-3">
 				<textarea class="form-control" id="details" name="details" height="200" placeholder="Description"></textarea>
 			<label for="details">Description</label>
 			</div>
 			
 			
 		<button type="submit" class="btn btn-navy float-end mt-3">Save</button>
 	</div>

 	
 		
	
 </form>

 <script type="text/javascript">
 	//end date for vacancy type notice (disability)
 	$(document).on('change','.form-select',function(){
 		// alert($(this).val())
 		if($(this).val() == 'vac'){
 			$('.endson').attr('disabled',false);
 		}
 		else{
 			$('.endson').attr('disabled',true);
 		}
 	})
 
 </script>