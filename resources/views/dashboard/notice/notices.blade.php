<?php 
use Illuminate\Support\Str;

 ?>

<h4 class="h4 mt-5">
	Notice List
</h4>
 <table class="table table-striped table-hover">
 	<thead>
 		<tr class="bg-secondary text-light mt-2">
 			<th>SN</th>
 			<th>Title</th>
 			<th>Type</th>
 			<th>Description</th>
 			<th>Published on</th>
 			<th>End Date</th>
 			<th>
 				Changes
 			</th>

 		</tr>
 	</thead>
 	<tbody>

 		@if(count($notice)>0)
 		@foreach($notice as $key=>$notice)
 		
 		<tr>
 			<td>{{$key+1}}</td>
 			<td>{{$notice->title}}</td>
 			<!-- <td>{{$notice->type}}</td> -->
 			<td>
 				@if($notice->type=='pro')
 					{{'Procurement'}}
 				@endif
 				@if($notice->type=='vac')
 					{{'Vacancy'}}
 				@endif
 				@if($notice->type=='press')
 					{{'Press Release'}}
 				@endif
 				@if($notice->type=='bill')
 					{{'Bill Presentation'}}
 				@endif
 				@if($notice->type=='pub')
 					{{'Public'}}
 				@endif
 			</td>
 
 			<td>{{substr($notice->details,0,100)}}</td>
 			<td>{{$notice->published_on}}</td>
 			<td>{{$notice->ends_on}}</td>
 			<td> <a href="" class="text-danger delete" data-id="{{$notice->id}}">Delete</a>|<a href="#" class="view text-warning" data-id="{{$notice->id}}" role="button">View</a></td>

 		</tr>
 		@endforeach
 		@else
 		{{'Nothing to show'}}
 		@endif

 	</tbody>
 </table>

 <!-- Modal for notice details -->
<div class="modal fade" id="notice" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">
        	
        		
        		
        		
        </h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="top">
        	<img src=""  id="myimg" style="height: 300px;"/>
        </div>
        <div class="bottom">
        	
        </div>
      </div>
      <div class="modal-footer">
      	<span class="published_on"></span>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

<!-- view contents on clicking view button -->
<script type="text/javascript">
	$(document).on('click','.view',function(){
		var id=$(this).data('id');
		var myModal = new bootstrap.Modal(document.getElementById('notice'), {
  keyboard: false
})
		$.ajax({
			url:"{{url('onenotice')}}",
			data:{id:id ,"_token": "{{ csrf_token() }}"},
			type:'post',
			dataType:'json',
			success: function(data){
				myModal.show();
				$('#notice .modal-title').html(data.onenotice.title);
				$('#notice .modal-body .bottom').html(data.onenotice.details);
				$('#myimg').attr('src',  data.onenotice.image );


			}
		});
	})

	$(document).on('click','.delete',function(){
		var id=$(this).data('id');
		// alert(id)
	$.ajax({
			url:'delnotice',
			data:{id:id ,"_token": "{{ csrf_token() }}"},
			type:'post',
			
			success: function(data){
				alert('Deleted successfully');

			}
		});
	})
</script>
