<?php 
use Illuminate\Support\Str;

 ?>

<h4 class="h4 mt-5">
	Project List
</h4>
 <table class="table table-striped table-hover">
 	<thead>
 		<tr class="bg-secondary text-light mt-2">
 			<th>SN</th>
 			<th>Title</th>
 			<th>Type</th>
 			<th>Location</th>
 			<th>Description</th>
 			<th>Sector</th>
 			<th>
 				Changes
 			</th>

 		</tr>
 	</thead>
 	<tbody>

 		@if(count($project)>0)
 		@foreach($project as $key=>$project)
 		
 		<tr>
 			<td>{{$key+1}}</td>
 			<td>{{$project->title}}</td>
 			<!-- <td>{{$project->type}}</td> -->
 			<td>
 				@if($project->type=='imp')
 					{{'Under Implementation'}}
 				@endif
 				@if($project->type=='sur')
 					{{'Under Survey'}}
 				@endif
 				@if($project->type=='pro')
 					{{'Under Procurement'}}
 				@endif
 				@if($project->type=='sol')
 					{{'Unsolicited'}}
 				@endif
 				@if($project->type=='inv')
 					{{'Investment'}}
 				@endif
 			</td>
 			<td>{{$project->location}}</td>
 	
 			<td>{{substr($project->details,0,100)}}</td>
 			<td>{{$project->sector}}</td>
 		
 			<td> <a href="" class="text-danger delete" data-id="{{$project->id}}">Delete</a>|<a href="#" class="view text-warning" data-id="{{$project->id}}" role="button">View</a></td>

 		</tr>
 		@endforeach
 		@else
 		{{'Nothing to show'}}
 		@endif

 	</tbody>
 </table>

 <!-- Modal for project details -->
<div class="modal fade" id="project" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">
        	
        		
        		
        		
        </h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="top">
        	<img src=""  id="myimg" style="height: 300px;"/>
        </div>
        <div class="bottom">
        	
        </div>
      </div>
      <div class="modal-footer">
      	<span class="published_on"></span>
      
      	<span class="investment"></span>
      	<span class="approved_on"></span>
      		<span class="initiated_by"></span>


        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

<!-- view contents on clicking view button -->
<script type="text/javascript">
	$(document).on('click','.view',function(){
		var id=$(this).data('id');
		var myModal = new bootstrap.Modal(document.getElementById('project'), {
  keyboard: false
})
		$.ajax({
			url:"{{url('oneproject')}}",
			data:{id:id ,"_token": "{{ csrf_token() }}"},
			type:'post',
			dataType:'json',
			success: function(data){
				myModal.show();
				$('#project .modal-title').html(data.oneproject.title);
				$('#project .modal-body .bottom').html(data.oneproject.details);
				$('#project .modal-footer .published_on').html("<b>Published on: </b>"+data.oneproject.published_on);
				$('#project .modal-footer .investment').html("<b>Total Investment: </b>"+data.oneproject.investment);
				$('#project .modal-footer .approved_on').html("<b>Approved on: </b>"+data.oneproject.approved_on);
				$('#project .modal-footer .initiated_by').html("<b>Initiated by: </b>"+data.oneproject.initiated_by);
				

				$('#myimg').attr('src',  data.oneproject.images );


			}
		});
	})

	$(document).on('click','.delete',function(){
		var id=$(this).data('id');
		// alert(id)
	$.ajax({
			url:'delproject',
			data:{id:id ,"_token": "{{ csrf_token() }}"},
			type:'post',
			
			success: function(data){
				alert('Deleted successfully');

			}
		});
	})
</script>
