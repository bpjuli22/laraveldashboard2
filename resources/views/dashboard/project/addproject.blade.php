<?php 
//### add project page with a form
 ?>



   <h1 class="h2 my-2">Add project</h1>
 <form class="form " action="/addproject" method="post" enctype="multipart/form-data">
 	@csrf
 	<div class="col-lg-10">
 		<div class="form-floating mb-3">
 			<input class="form-control" id="title" type="text" placeholder="Title" name="title" />
 		<label for="title">Title for project</label>
 		</div>

 		<div class="row">
 			<div class="col-6">
 			<select class="form-select" name="type">
 				<option value=" ">--project type--</option>
 				<option value="imp">Under Implementation</option>
 				<option value="sur">Under Survey</option>
 				<option value="pro">Under Procurement</option>
 				<option value="sol">Unsolicited</option>
  				<option value="inv">Investment</option>
 				

 			</select>
 		</div>
 		<div class="col-6">
 		<input class="form-file mt-1" type="file" name="img" />
 		</div>
 		</div>
 		
 	
 			<div class="form-floating mt-3">
 				<textarea class="form-control" id="details" name="details" height="200" placeholder="Description"></textarea>
 			<label for="details">Description</label>
 			</div>
 			<div class="row mt-3">
 			<div class="col-6">
 			<div class="form-floating" >
 			
 			<input class="form-control location" type="text" id="location" name="location" placeholder="location" />
 			<label for="location" class="form-label">Location</label>
 			</div>
 				</div>
 			<div class="col-6">
	 			<div class="form-floating" >
	 			
	 			<input class="form-control sector" type="text" id="sector" name="sector" placeholder="sector" />
	 			<label for="sector" class="form-label">Sector</label>
	 			</div>
 			
 			</div>
 			</div>

 			<div class="row mt-3">
 				<div class="col-5 mt-3">
 			<div class="form-floating" >
  			<input class="form-control initiatedBy" type="text" id="initiatedBy" name="init" placeholder="initiatedBy" />
 			<label for="initiatedBy" class="form-label">Initiated By</label>
 			</div>
 			
 				</div>
 				<div class="col-4 mt-3">
 			<div class="form-floating" >
  			<input class="form-control budget" type="number" id="budget" name="budget" placeholder="budget" min="1000" />
 			<label for="budget" class="form-label">Investment Amount</label>
 			</div>
 			
 				</div>
	 			<div class="col-3">
		 			<div class="" >
		 			<label for="approvedOn" class="form-label pt-1">Approval Date</label>
		 			<input class="form-control approvedOn" type="date" id="approvedOn" name="approved_on" placeholder="Approved on" />
		 			</div>
 				</div>
 			</div>
 			
 		<button type="submit" class="btn btn-navy float-end mt-3">Save</button>
 	</div>

 	
 		
	
 </form>

 <script type="text/javascript">
 	//disability of form for except investments
 	$(document).on('change','.form-select',function(){
 		// alert($(this).val())
 		if($(this).val() == 'inv'){
 			$('#initiatedBy').attr('disabled',false);
 			$('#approvedOn').attr('disabled',false);
 			$('#budget').attr('disabled',false);
 			$('#sector').attr('disabled',false);


 		}
 		else{
 			$('#initiatedBy').attr('disabled',true);
 			$('#approvedOn').attr('disabled',true);
 			$('#budget').attr('disabled',true);
 			$('#sector').attr('disabled',true);

 		}
 	})
 </script>