<?php 
use Illuminate\Support\Str;

 ?>

<h4 class="h4 mt-5">
	Staff List
</h4>
 <table class="table table-striped table-hover">
 	<thead>
 		<tr class="bg-secondary text-light mt-2">
 			<th>SN</th>
 			<th>Full Name</th>
 			<th>Type</th>
 			<th>Designation</th>
 			<th>Institution</th>
 			<th>Message</th>
 			<th>
 				Changes
 			</th>

 		</tr>
 	</thead>
 	<tbody>

 		@if(count($staff)>0)
 		@foreach($staff as $key=>$staff)
 		
 		<tr>
 			<td>{{$key+1}}</td>
 			<td>{{$staff->fullname}}</td>
 			<!-- <td>{{$staff->type}}</td> -->
 			<td>
 			<!-- 	@if($staff->type=='imp')
 					{{'Under Implementation'}}
 				@endif
 				@if($staff->type=='sur')
 					{{'Under Survey'}}
 				@endif
 				@if($staff->type=='pro')
 					{{'Under Procurement'}}
 				@endif
 				@if($staff->type=='sol')
 					{{'Unsolicited'}}
 				@endif
 				@if($staff->type=='inv')
 					{{'Investment'}}
 				@endif -->
 				{{$staff->type}}
 			</td>
 			<td>
 				@if($staff->type=='mem')
 					{{'Member'}}
 				@endif
 				@if($staff->type=='bmem')
 					{{'Board Member'}}
 				@endif
 			</td>
 			<td>{{$staff->institution}}</td>
 			<td>{{substr($staff->message,0,100)}}</td>
 			
 		
 			<td> <a href="" class="text-danger delete" data-id="{{$staff->id}}">Delete</a>|<a href="#" class="view text-warning" data-id="{{$staff->id}}" role="button">View</a></td>

 		</tr>
 		@endforeach
 		@else
 		{{'Nothing to show'}}
 		@endif

 	</tbody>
 </table>

 <!-- Modal for staff details -->
<div class="modal fade" id="staff" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">
        	
        		
        		
        		
        </h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="top">
        	<img src=""  id="myimg" style="height: 300px;"/>
        </div>
        <div class="bottom">
        	
        </div>
      </div>
      <div class="modal-footer">
      	<span class="published_on"></span>
         <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

<!-- view contents on clicking view button -->
<script type="text/javascript">
	$(document).on('click','.view',function(){
		var id=$(this).data('id');
		var myModal = new bootstrap.Modal(document.getElementById('staff'), {
  keyboard: false
})
		$.ajax({
			url:"{{url('onestaff')}}",
			data:{id:id ,"_token": "{{ csrf_token() }}"},
			type:'post',
			dataType:'json',
			success: function(data){
				myModal.show();
				$('#staff .modal-title').html(data.onestaff.fullname);
				$('#staff .modal-body .bottom').html(data.onestaff.message);
				$('#staff .modal-footer .published_on').html("<b>Registered on: </b>"+data.onestaff.registered_on);
				
				

				$('#myimg').attr('src', data.onestaff.photo);


			}
		});
	})

	$(document).on('click','.delete',function(){
		var id=$(this).data('id');
		// alert(id)
	$.ajax({
			url:'delstaff',
			data:{id:id ,"_token": "{{ csrf_token() }}"},
			type:'post',
			
			success: function(data){
				alert('Deleted successfully');

			}
		});
	})
</script>
