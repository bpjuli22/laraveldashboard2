<?php 
//### add staff page with a form
 ?>



   <h1 class="h2 my-2">Add staff</h1>
 <form class="form " action="/addstaff" method="post" enctype="multipart/form-data">
 	@csrf
 	<div class="col-lg-10">
 		<div class="form-floating mb-3">
 			<input class="form-control" id="fullname" type="text" placeholder="fullname" name="name" />
 		<label for="fullname">Full Name</label>
 		</div>

 		<div class="row">
 			<div class="col-6">
 			<select class="form-select type" name="type">
 				<option value=" ">--staff type--</option>
 				<option value="staff">Staff</option>
 				<option value="mem">Member</option>
 				<option value="bmem">Board Member</option>
 				
 				

 			</select>
 		</div>
 		<div class="col-6">
 			<label>Photo</label>
 		<input class="form-file mt-1" type="file" name="img" />
 		</div>
 		</div>
 		 			<div class="row mt-3">
 			<div class="col-6">
 			<select class="form-select" name="des">
 				<option value=" ">--post/designation--</option>
 				<option value="pia">PIA</option>
 				<option value="consultant">Consultant</option>
 				<option value="chairperson">Chairperson</option>
 				<option value="ceo">CEO</option>

 				
 				

 			</select>
 				</div>
 				<div class="col-6">
 			<label>Document</label>
 		<input class="form-file mt-1" type="file" name="doc" />
 		</div>
 			
 			</div>
 	
 			<div class="form-floating mt-3">
 				<textarea class="form-control" id="message" name="msg" height="200" placeholder="Message"></textarea>
 			<label for="message">Message</label>
 			</div>


 			<div class="mt-3">
	 			<div class="form-floating" >
	 			
	 			<input class="form-control institution" type="text" id="institution" name="inst" placeholder="institution" />
	 			<label for="institution" class="form-label">Institution</label>
	 			</div>
 			
 			</div>
 			
 		<button type="submit" class="btn btn-navy float-end mt-3">Save</button>
 	</div>

 	
 		
	
 </form>

 <script type="text/javascript">
 	//end date for vacancy type staff (disability)
 	$(document).on('change','.type',function(){
 		// alert($(this).val())
 		if($(this).val() == 'bmem'){
 			$('#message').attr('disabled',false);
 		}
 		else{
 			$('#message').attr('disabled',true);
 		}
 	})
 </script>