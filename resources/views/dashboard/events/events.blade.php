<?php 
// all events list
 ?>

 <?php 
use Illuminate\Support\Str;

 ?>

<h4 class="h4 mt-5">
	Event List
</h4>
 <table class="table table-striped table-hover">
 	<thead>
 		<tr class="bg-secondary text-light mt-2">
 			<th>SN</th>
 			<th>Title</th>
 			<th>Guest</th>
 			<th>location</th>
 			<th>Description</th>
 			<th>Published on</th>
 			<th>Event Date</th>
 			<th>Event Time</th>

 			<th>
 				Changes
 			</th>

 		</tr>
 	</thead>
 	<tbody>

 		@if(count($event)>0)
 		@foreach($event as $key=>$event)
 		
 		<tr>
 			<td>{{$key+1}}</td>
 			<td>{{$event->title}}</td>
 			<td>{{$event->guest}}</td>
 			<td>{{$event->location}}</td>


 		
 
 			<td>{{substr($event->details,0,100)}}</td>
 			<td>{{$event->published_on}}</td>
 			<td>{{$event->event_date}}</td>
 			<td>{{$event->event_time}}</td>

 			<td> <a href="" class="text-danger delete" data-id="{{$event->id}}">Delete</a>|<a href="#" class="view text-warning" data-id="{{$event->id}}" role="button">View</a></td>

 		</tr>
 		@endforeach
 		@else
 		{{'Nothing to show'}}
 		@endif

 	</tbody>
 </table>

 <!-- Modal for notice details -->
<div class="modal fade" id="event" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">
        	
        		
        		
        		
        </h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="top">
        	<img src=""  id="myimg" />
        </div>
        <div class="bottom">
        	
        </div>
      </div>
      <div class="modal-footer">
      	<span class="published_on"></span>
      	<span class="guest"></span>
      
      	<span class="startdate"></span>
      	<span class="enddate"></span>
      		<span class="starttime"></span>
      		<span class="endtime"></span>

        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

<!-- view contents on clicking view button -->
<script type="text/javascript">
	$(document).on('click','.view',function(){
		var id=$(this).data('id');
		var myModal = new bootstrap.Modal(document.getElementById('event'), {
  keyboard: false
})
		$.ajax({
			url:"{{url('oneevent')}}",
			data:{id:id ,"_token": "{{ csrf_token() }}"},
			type:'post',
			dataType:'json',
			success: function(data){
				myModal.show();
				$('#event .modal-title').html(data.oneevent.title);
				$('#event .modal-body .bottom').html(data.oneevent.details);
				$('#project .modal-footer .published_on').html("<b>Guest: </b>"+data.oneevent.published_on);
				$('#event .modal-footer .guest').html("<b>Guest: </b>"+data.oneevent.guest);
				$('#event .modal-footer .startdate').html("<b>Start from: </b>"+data.oneevent.event_date);
				$('#event .modal-footer .enddate').html("<b>End on: </b>"+data.oneevent.end_date);
				$('#event .modal-footer .starttime').html("<b>Start time: </b>"+data.oneevent.event_time);
				$('#event .modal-footer .endtime').html("<b>End time: </b>"+data.oneevent.end_time);

				$('#myimg').attr('src',  data.oneevent.image);
				


			}
		});
	})

	$(document).on('click','.delete',function(){
		var id=$(this).data('id');
		// alert(id)
	$.ajax({
			url:'delevent',
			data:{id:id ,"_token": "{{ csrf_token() }}"},
			type:'post',
			
			success: function(data){
				alert('Deleted successfully');

			}
		});
	})
</script>
