<?php 
//### add event page with form
 ?>

   <h1 class="h2 my-2">Add Event</h1>
 <form class="form " action="/addevent" method="post" enctype="multipart/form-data">
 	@csrf
 	<div class="col-lg-10">
 		<div class="form-floating mb-3">
 			<input class="form-control" id="title" type="text" placeholder="Title" name="title" />
 		<label for="title">Title for event</label>
 		</div>

 		<div class="form-floating">
 			<input class="form-control" placeholder="guest" id="guest" name="guest" />
 			<label for="guest">Guest Name</label>
 		</div>
 		<div class="row">
 			<div class="col-6 ">
 					<div class="form-floating mt-2">
 						<input class="eventdate form-control" type="text" id="location" name="location" placeholder="location" />
 					<label for="location" class="form-label ">Event Location</label>
	 			
	 			</div>
 				</div>
 				<div class="col-6 mt-3">
 					<label>Image</label>
 				<input class="form-file mt-1" type="file" name="img" />
 				</div>
 		</div>
 		
 	
 			<div class="form-floating mt-3">
 				<textarea class="form-control" id="details" name="details" height="200" placeholder="Description"></textarea>
 			<label for="details">Description</label>
 			</div>

 			<div class="row mt-3">
 				
	 			<div class="col-lg-3" >
	 			<label for="eventdate" class="form-label ">Event Date</label>
	 			<input class="eventdate col-8 form-control" type="date" id="eventdate" name="eventdate" placeholder="event date" />
	 			</div>
	 			<div class="col-lg-3" >
	 			<label for="eventdate" class="form-label ">End Date</label>
	 			<input class="eventdate col-8 form-control" type="date" id="enddate" name="enddate" placeholder="event date" />
	 			</div>
	 			<div class="col-lg-3" >
	 			<label for="eventtime" class="form-label ">Event Time</label>
	 			<input class="eventtime form-control" type="time" id="eventtime" name="eventtime" placeholder="event time" />
	 			</div>
	 			<div class="col-lg-3" >
	 			<label for="eventtime" class="form-label ">End Time</label>
	 			<input class="eventtime form-control" type="time" id="endtime" name="endtime" placeholder="event time" />
	 			</div>
 			</div>
 			
 		<button type="submit" class="btn btn-navy float-end mt-3">Save</button>
 	</div>

 	
 		
	
 </form>

 <script type="text/javascript">
 	
 	$(document).on('click','#guest',function(){

 	})
 </script>