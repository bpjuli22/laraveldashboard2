<?php

namespace App\Http\Controllers;
// namespace App\Models;

// upload images
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use App\Models\Notice;
use App\Models\Event;
use App\Models\News;
use App\Models\Project;
use App\Models\Staff;



use Carbon\Carbon;

class PostController extends Controller
{
    function addNotice(Request $req){
    	$title=$req->input('title');
    	$type=$req->input('type');
    	 $img=$req->file('img');
        $upload=!empty($img)?$img->storeAs('public',$img->getClientOriginalName()):'-';  
        $url = Storage::url($img->getClientOriginalName());
    	$details=$req->input('details');
    	$endson=$req->input('endson');
    	
    	$ins=Notice::insert([
    		'title' => $title,
    		'type' => $type,
    		'image' => $url,
    		'details' => $details,
    		'published_on' => date('Y/m/d H:i:s'),
    		'ends_on' => !empty($endson)?$endson:date('Y/m/d')

    	]);

    	if($ins){
    		// return response()->flash('msg', 'Notice added successfully');
    		return redirect()->back()->with(['msg' => 'Notice added successfully','bg' => 'success']);
    	}
    	else{
    		return back()->with(['err' => 'Something is wrong','bg' => 'danger']);
    	}


    }

     function addEvent(Request $req){
        $title=$req->input('title');
        $details=$req->input('details');
        $guest=$req->input('guest');
         $img=$req->file('img');
        $upload=!empty($img)?$img->storeAs('public',$img->getClientOriginalName()):'-';  
        $url = Storage::url($img->getClientOriginalName());
        $eventdate=$req->input('eventdate');
        $eventtime=$req->input('eventtime');
        $enddate=$req->input('enddate');
        $endtime=$req->input('endtime');

        $location=$req->input('location');

        
        $ins=Event::insert([
            'title' => $title,
            'guest' => $guest,
            'image' => $url,
            'details' => $details,
            'location' => $location,
            'published_on' => date('Y-m-d H:i:s'),
            'event_date' => $eventdate ,
            'end_date' => $enddate,
            'end_time' => $endtime,
            // 'event_time' => Carbon::parse($eventtime)->format('H:i:00')
            'event_time' => $eventtime

        ]);

        if($ins){
            // return response()->flash('msg', 'Notice added successfully');
            return redirect()->back()->with(['msg' => 'Event added successfully','bg' => 'success']);
        }
        else{
            return back()->with(['err' => 'Something is wrong','bg' => 'danger']);
        }


    }

    function addNews(Request $req){
        $title=$req->input('title');
        // $type=$req->input('type');
        $img=$req->file('img');
        $upload=!empty($img)?$img->storeAs('public',$img->getClientOriginalName()):'-';  
        $url = Storage::url($img->getClientOriginalName());
        $details=$req->input('details');
 
        
        $ins=News::insert([
            'title' => $title,
            // 'type' => $type,
            'image' => $url,
            'details' => $details,
            'published_on' => date('Y/m/d H:i:s'),
         

        ]);

        if($ins){
            // return response()->flash('msg', 'Notice added successfully');
            return redirect()->back()->with(['msg' => 'News added successfully','bg' => 'success']);
        }
        else{
            return back()->with(['err' => 'Something is wrong','bg' => 'danger']);
        }


    }

 function addProject(Request $req){
        $title=$req->input('title');
        $type=$req->input('type');
       $img=$req->file('img');
        $upload=!empty($img)?$img->storeAs('public',$img->getClientOriginalName()):'-';  
        $url = Storage::url($img->getClientOriginalName());
        $details=$req->input('details');
        $location=$req->input('location');
        $sector=$req->input('sector');
        $investment=$req->input('investment');
        $approved_on=$req->input('approved_on');
        $init=$req->input('init');



        
        $ins=Project::insert([
            'title' => $title,
            'type' => $type,
            'images' => $url,
            'details' => $details,
            'location' => $location,
            'sector' => $sector,
            'investment' => $investment,
            'approved_on' => $approved_on,
            'initiated_by' => $init,
            'status' => 0, 
            'published_on' => date('Y/m/d H:i:s'),
           

        ]);

        if($ins){
            // return response()->flash('msg', 'Notice added successfully');
            return redirect()->back()->with(['msg' => 'Project added successfully','bg' => 'success']);
        }
        else{
            return back()->with(['err' => 'Something is wrong','bg' => 'danger']);
        }


    }

function addStaff(Request $req){
        $fn=$req->input('name');
        $type=$req->input('type');

        $img=$req->file('img');
        $upload=!empty($img)?$img->storeAs('public',$img->getClientOriginalName()):'-';  
        $url = Storage::url($img->getClientOriginalName());

        $msg=$req->input('msg');
        $doc=!empty($req->file('doc'))?$req->file('doc')->store('images'):'-';
        $des=$req->input('des');
        $inst=$req->input('inst');
        $ins=Staff::insert([
            'fullname' => $fn,
            'type' => $type,
            'photo' => $url,
            'document' => $doc,
            'message' => $msg,
            'designation' => $des,
            'institution' => $inst,
            'registered_on' => date('Y/m/d H:i:s'),
           

        ]);

        if($ins){
            // return response()->flash('msg', 'Notice added successfully');
            return redirect()->back()->with(['msg' => 'Staff added successfully','bg' => 'success']);
        }
        else{
            return back()->with(['err' => 'Something is wrong','bg' => 'danger']);
        }


    }
}
