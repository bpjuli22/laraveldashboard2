<?php

namespace App\Http\Controllers;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;
use App\Models\Notice;
use App\Models\Event;
use App\Models\News;
use App\Models\Project;
use App\Models\Staff;






class GetController extends Controller
{

    /*User Interface */
    function homePage(){
        $event=Event::orderBy('event_date','desc')->limit(7)->get();
        $bmem=Staff::where('type','bmem')->get();
        $not=Notice::orderBy('published_on','desc')->limit(12)->get();
        $news=News::orderBy('published_on','desc')->limit(7)->offset(4)->get();
        $fnews=News::orderBy('published_on','desc')->limit(4)->get();
        $pro=Project::orderBy('published_on','desc')->limit(10)->get();
       

        return view('welcome',['event' => $event,'bmem' => $bmem,'not' => $not,'news' => $news,'pro' => $pro, 'fnews' => $fnews]);
    }


    function singleNotice(Request $req, $id){
        // $id=$req->input('id');
            $not=Notice::find($id);
          $fnews=News::orderBy('published_on','desc')->limit(4)->get();
            return view('notice',['not' => $not,'fnews' => $fnews]);
    }
    function noticeList(){
        $allnot=Notice::paginate(10);
        $fnews=News::orderBy('published_on','desc')->limit(4)->get();
        // var_dump($allnot);die;
        return view('noticelist',['all' => $allnot,'fnews' => $fnews]);
    }

    function eventList(){
        $alleve=Event::paginate(9);
        $fnews=News::orderBy('published_on','desc')->limit(4)->get();
        // var_dump($alleve);die;
        return view('eventlist',['all' => $alleve,'fnews' => $fnews]);
    }


     function singleEvent(Request $req, $id){
            $eve=Event::find($id);
          $fnews=News::orderBy('published_on','desc')->limit(4)->get();
            return view('event',['eve' => $eve,'fnews' => $fnews]);
    }

    function newsList(){
        $allnews=News::paginate(9);
        $fnews=News::orderBy('published_on','desc')->limit(4)->get();
        // var_dump($allnews);die;
        return view('newslist',['all' => $allnews,'fnews' => $fnews]);
    }


     function singleNews(Request $req, $id){
            $news=News::find($id);
            $lnews=News::where('id','<>',$id)->orderBy('published_on','desc')->limit(7)->get();
          $fnews=News::orderBy('published_on','desc')->limit(4)->get();
            return view('news',['news' => $news,'fnews' => $fnews, 'lnews' => $lnews]);
    }

     function projectList(){
        $allpro=Project::paginate(8);
        $fnews=News::orderBy('published_on','desc')->limit(4)->get();
        // var_dump($allpro);die;
        return view('projectlist',['all' => $allpro,'fnews' => $fnews]);
    }


     function singleProject(Request $req, $id){
            $pro=Project::find($id);
            $lpro=Project::orderBy('published_on','desc')->limit(12)->get();
          $fnews=News::orderBy('published_on','desc')->limit(4)->get();
            return view('project',['pro' => $pro,'fnews' => $fnews, 'lpro' => $lnews]);
    }

    // function footer(){
    //      $fnews=News::orderBy('published_on','desc')->limit(4)->get();
      

    //     return view('layouts.app',['fnews' => $fnews]);
    // }

    /* Dashboard */
    //### Notices
    function getNotice(Request $req){
    	
    	$notice=Notice::orderBy('id','desc')->get();
    
    	return view('dashboard/notice/notices',['notice' => $notice]);
    }


    function oneNot(Request $req){
    	$id=$req->input('id');
    		$onenotice=Notice::find($id);
    		return response()->json(['onenotice' => $onenotice]);
    }

    //### Events
    function getEvent(Request $req){
        
        $event=Event::orderBy('id','desc')->get();
    
        return view('dashboard/events/events',['event' => $event]);
    }
    function oneEve(Request $req){
        $id=$req->input('id');
            $oneevent=Event::find($id);
            return response()->json(['oneevent' => $oneevent]);
    }

    //### News
    function getNews(Request $req){
        
        $news=News::orderBy('id','desc')->get();
    
        return view('dashboard/news/news',['news' => $news]);
    }
    function oneNews(Request $req){
        $id=$req->input('id');
            $onenews=News::find($id);
            return response()->json(['onenews' => $onenews]);
    }

     //### Projects
    function getProject(Request $req){
        
        $project=Project::orderBy('id','desc')->get();
    
        return view('dashboard/project/projects',['project' => $project]);
    }
    function oneProject(Request $req){
        $id=$req->input('id');
            $oneproject=Project::find($id);
            return response()->json(['oneproject' => $oneproject]);
    }

    //### Projects
    function getStaff(Request $req){
        
        $staff=Staff::orderBy('id','desc')->get();
    
        return view('dashboard/staff/staffs',['staff' => $staff]);
    }
    function oneStaff(Request $req){
        $id=$req->input('id');
            $onestaff=Staff::find($id);
            return response()->json(['onestaff' => $onestaff]);
    }

}
