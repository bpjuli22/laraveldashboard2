<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Notice;
use App\Models\Event;
use App\Models\News;
use App\Models\Project;
use App\Models\Staff;





class DeleteController extends Controller
{
    function delNotice(Request $req){
    	$id=$req->input('id');
    	$del=Notice::where('id',$id)->delete();
    	if($del){
    		return true;
    	}
    }

     function delEvent(Request $req){
    	$id=$req->input('id');
    	$del=Event::where('id',$id)->delete();
    	if($del){
    		return true;
    	}
    }

     function delNews(Request $req){
        $id=$req->input('id');
        $del=News::where('id',$id)->delete();
        if($del){
            return true;
        }
    }

     function delProject(Request $req){
        $id=$req->input('id');
        $del=Project::where('id',$id)->delete();
        if($del){
            return true;
        }
    }
    function delStaff(Request $req){
        $id=$req->input('id');
        $del=Staff::where('id',$id)->delete();
        if($del){
            return true;
        }
    }
}
